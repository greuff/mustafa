FROM python:3.10-bullseye AS builder
RUN apt-get update \
    && apt-get install -y --no-install-recommends python3-dev \
    && apt-get clean
RUN pip3 install pipenv
RUN mkdir /usr/src/app
WORKDIR /usr/src/app
COPY Pipfile Pipfile.lock /usr/src/app/
# COPY *.py /usr/src/app/
COPY mustafa /usr/src/app/mustafa
RUN pipenv install

FROM python:3.10-bullseye
RUN apt-get update \
    && apt-get install -y --no-install-recommends vim-tiny \
    && apt-get clean
RUN pip3 install pipenv
COPY --from=builder /usr/src/app /usr/src/app
COPY --from=builder /root/.local/ /root/.local/
WORKDIR /usr/src/app
CMD ["pipenv", "run", "python3", "-m", "mustafa", "/data/services"]
