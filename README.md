# Mustafa

Mustafa is a lightweight Python-based monitoring solution that checks healthiness of services by running periodic check scripts and notifying you about problems.

A few lines of code are enough to define your service, check and notification:

```python
class Service(mustafa.BaseService):
    def __init__(self):
        super().__init__(__name__)
        self.notifiers = [LoggingNotifier()]
        schedule.every(10).minutes.do(self.check)

    @mustafa.service(cancel_on_failure=False)
    def check(self):
        return check_https_expiration(self.logger, 'www.google.com', 443)
```

Mustafa takes care of repeatedly calling your check and notifying you if anything went wrong.

It comes with a growing set of check helpers and notification classes, but it's very easy to define your own. The whole power of the Python ecosystem is available to you.

## Running Mustafa

All you need is the `mustafa` module and a directory with your service check scripts (like in the example above). Suppose you put all your service check scripts 
into a `services` directory somewhere. Then simply run:

```shell
$ python3 -m mustafa services
```

You can also run Mustafa from Docker like this:

```shell
$ docker run --name mustafa -v `pwd`/scripts:/data/services mustafa:latest
```
