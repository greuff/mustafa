import schedule
import mustafa
from mustafa.checkers.ssl.https import check_https_expiration
from mustafa.notifiers.slack.notify_slack import SlackNotifier


class Service(mustafa.BaseService):

	def __init__(self):
		super().__init__(__name__)
		self.notifiers = [SlackNotifier()]
		schedule.every(3).seconds.do(self.check)

	@mustafa.service(cancel_on_failure=False)
	def check(self):
		return check_https_expiration(self.logger, 'expired.badssl.com', 443)
