import mustafa
import logging
import os
from ...checkers import CheckResult
from slack_sdk import WebClient


class SlackNotifier(mustafa.BaseNotifier):
	"""
	A Mustafa Notifier that can post to Slack.
	Needs envs:
	   SLACK_BOT_TOKEN: the OAuth token
	   SLACK_CHANNEL: the channel to post to (as channel id)
	"""
	def __init__(self):
		super().__init__(__name__)
		self.logger = logging.getLogger(__name__)
		slack_token = os.environ["SLACK_BOT_TOKEN"]
		self.slack_channel = os.environ["SLACK_CHANNEL"]
		proxy = None
		if 'PROXY_HTTPS_HOST' in os.environ and 'PROXY_HTTPS_PORT' in os.environ:
			proxy = f"http://{os.environ['PROXY_HTTPS_HOST']}:{os.environ['PROXY_HTTPS_PORT']}"
		self.client = WebClient(token=slack_token, proxy=proxy)
		self.client.api_test()

	def send(self, check_result: CheckResult):
		self.client.chat_postMessage(
			channel=self.slack_channel,
			text=f"{mustafa.config['instance_name']}: {check_result.message}"
		)

	def send_text(self, text):
		self.client.chat_postMessage(
			channel=self.slack_channel,
			text=f"{mustafa.config['instance_name']}: {text}"
		)
