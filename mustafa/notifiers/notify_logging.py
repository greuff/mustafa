import mustafa
import logging
from ..checkers import CheckResult


class LoggingNotifier(mustafa.BaseNotifier):
	def __init__(self):
		super().__init__(__name__)
		self.logger = logging.getLogger(__name__)

	def send(self, check_result: CheckResult):
		self.logger.error(f"{mustafa.config['instance_name']}: {check_result.message}")

	def send_text(self, text: str):
		self.logger.info(f"{mustafa.config['instance_name']}: {text}")
