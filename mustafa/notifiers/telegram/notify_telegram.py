import mustafa
import telegram_send

from ...checkers import CheckResult


class TelegramNotifier(mustafa.BaseNotifier):
	"""
	Notifier via telegram.

	This needs a config file somewhere with the following contents:
	[telegram]
	token = xxxx
	chat_id = yyyy
	"""

	def __init__(self, config_file):
		super().__init__(__name__)
		self.config_file = config_file

	def send(self, check_result: CheckResult):
		telegram_send.send(messages=[f"{mustafa.config['instance_name']}: {check_result.message}"],
						   conf=self.config_file)

	def send_text(self, text: str):
		telegram_send.send(messages=[f"{mustafa.config['instance_name']}: {text}"],
						   conf=self.config_file)
