import glob
import logging
import os
import schedule
import time
import click
import mustafa
import sys
import signal


def sighup_handler(sig, frame):
	"""
	SIGHUP handler: run all checks immediately.
	:param sig:
	:param frame:
	:return:
	"""
	schedule.run_all()


@click.command()
@click.option('-n', '--instance-name', type=str, default="mustafa", show_default=True, help="The name of the instance in notifications.")
@click.option('--run-immediately/--no-run-immediately', default=False, show_default=True, help="Whether to run all checks immediately upon startup")
@click.argument('services-path')
def main(services_path, instance_name, run_immediately):
	"""
	Periodically run all job scripts in SERVICES_PATH
	"""
	logging.basicConfig(level=logging.DEBUG, format="%(levelname)s:%(process)d:%(name)s: %(message)s")

	logger = logging.getLogger("main")
	logger.setLevel(logging.DEBUG)

	mustafa.config['instance_name'] = instance_name

	sys.path.append(services_path)
	for filename in sorted(glob.glob(f"{services_path}/*.py")):
		logging.info("Loading service {}.".format(filename))

		module = os.path.basename(filename).replace(".py", "")
		exec(f"""import {module} as s
try:
	# Try to instantiate a "Service" class in the loaded module.
	# Not every module needs to have such a class. But those that
	# have will be instantiated. Other modules might be helper modules. 
	m = s.Service()
except NameError:
	pass
except AttributeError:
	pass
""")
	logging.info("Startup complete. Waiting to execute scheduled services.")

	if run_immediately:
		schedule.run_all()
	signal.signal(signal.SIGHUP, sighup_handler)
	while True:
		schedule.run_pending()
		time.sleep(1)


main()
