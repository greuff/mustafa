import functools
from abc import abstractmethod
import schedule
import logging
import signal
from .checkers import CheckResult

# This contains global config that is available in all modules
config = {
	'instance_name': None
}


class TimeoutError(Exception):
	pass


def service(cancel_on_failure=False, timeout=60):
	"""
	A decorator for a Mustafa service.
	:param cancel_on_failure: Whether a check that raised an unhandled exception should be taken out of schedule.
	:param timeout: After how many seconds the check should be forcefully aborted.
	:return:
	"""

	def job_decorator(job_func):
		def _handle_timeout(signum, frame):
			raise TimeoutError(f"timeout {timeout}s exceeded")

		@functools.wraps(job_func)
		def wrapper(*args, **kwargs):
			service_instance = args[0]
			signal.signal(signal.SIGALRM, _handle_timeout)
			signal.alarm(timeout)
			try:
				service_instance.logger.debug("Starting check")
				# run the actual check method
				cr = job_func(*args, **kwargs)

				# handle the outcome
				service_instance.process_check_result(cr, service_instance.notifiers)
			except Exception as e:

				service_instance.logger.exception(e)

				# send notifications.
				try:
					for notifier in service_instance.notifiers:
						notifier.send(
							CheckResult(code=-999, message=f"{service_instance.name}: unhandled exception: {e}"))
				except Exception as ex:
					service_instance.logger.exception(ex)

				if cancel_on_failure:
					return schedule.CancelJob
			finally:
				signal.alarm(0)

		return wrapper

	return job_decorator


class BaseService:
	"""
	Each service maintains exactly one CheckResult.
	"""
	logger = None
	name = None
	previous_check_result = None

	def __init__(self, loggername):
		self.name = loggername
		self.logger = logging.getLogger(loggername)

	def process_check_result(self, cr, notifiers):
		"""
		Main check result handling method.
		:param cr:
		:param notifiers:
		:return:
		"""

		# Log the result
		if cr.code == 0:
			self.logger.info(f"OK: {cr.message}")
		elif cr.code > 0:
			self.logger.warning(f"{cr.message}")
		elif cr.code < 0:
			self.logger.error(f"{cr.message}")

		if self.previous_check_result is not None:
			if self.previous_check_result.code == cr.code:
				# Nothing changed in the check result
				if cr.code != 0:
					self.logger.warning(f"check is in failed state, but not notifying again")
				return

		# Do we have to notify someone?
		# Only notify when the result code of a check changes to non-success.
		if cr.code != 0:
			for notifier in notifiers:
				checkResult = CheckResult(code=cr.code, message=f"{self.name}: {cr.message}")
				notifier.send(checkResult)
		else:
			self.logger.debug("check recovered.")

		self.previous_check_result = cr


class BaseNotifier:
	logger = None

	def __init__(self, loggername):
		self.logger = logging.getLogger(loggername)

	@abstractmethod
	def send(self, check_result: CheckResult):
		pass

	@abstractmethod
	def send_text(self, text: str):
		pass
