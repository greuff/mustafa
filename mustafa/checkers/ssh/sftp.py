from mustafa.checkers import CheckResult
import paramiko
import logging

logging.getLogger("paramiko.transport").setLevel(level=logging.WARN)


def check_sftp_file(hostname: str,
					remote_filename: str,
					username: str,
					password: str = None,
					private_key_file: str = None,
					minimum_file_size: int = None) -> CheckResult:
	"""
	Checks over ssh if a file exists on the remote machine and optionally for certain properties.
	:return:
	"""

	# Connect
	pk = None
	if private_key_file is not None:
		pk = paramiko.RSAKey.from_private_key(open(private_key_file))
	with paramiko.Transport(hostname) as transport:
		transport.connect(username=username, password=password, pkey=pk)
		sftp = paramiko.SFTPClient.from_transport(transport)

		# Check if the file is there at all
		try:
			filestat = sftp.stat(remote_filename)
		except FileNotFoundError:
			return CheckResult(code=-1, message=f"file {remote_filename} not found on {hostname}!")

		# Check if the file is at least minimum_file_size bytes large
		if minimum_file_size is not None and filestat.st_size < minimum_file_size:
			return CheckResult(code=-2,
							   message=f"file {remote_filename} on {hostname} is smaller than {minimum_file_size}: {filestat.st_size}!")

		return CheckResult(code=0, message=f"file {remote_filename} checked OK on {hostname}")
