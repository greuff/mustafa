from OpenSSL import SSL
import idna
import datetime
from socket import socket
from mustafa.checkers import CheckResult


def check_https_expiration(logger, hostname, port, days_left_warning_threshold=30,
						   days_left_critical_threshold=10) -> CheckResult:
	hostname_idna = idna.encode(hostname)
	sock = socket()

	sock.connect((hostname, port))
	ctx = SSL.Context(SSL.SSLv23_METHOD)  # most compatible
	ctx.check_hostname = False
	ctx.verify_mode = SSL.VERIFY_NONE

	sock_ssl = SSL.Connection(ctx, sock)
	sock_ssl.set_connect_state()
	sock_ssl.set_tlsext_host_name(hostname_idna)
	sock_ssl.do_handshake()
	cert = sock_ssl.get_peer_certificate()
	crypto_cert = cert.to_cryptography()
	sock_ssl.close()
	sock.close()

	days_left = (crypto_cert.not_valid_after - datetime.datetime.now()).days

	if cert.has_expired() or days_left <= days_left_critical_threshold:
		return CheckResult(code=-1, message=f"Certificate for {hostname} has expired on {crypto_cert.not_valid_after}!")
	else:
		if days_left <= days_left_warning_threshold:
			return CheckResult(code=1,
							   message=f"Certificate for {hostname} expires on {crypto_cert.not_valid_after} (days left: {days_left})")
		else:
			return CheckResult(code=0,
							   message=f"Certificate for {hostname} expires on {crypto_cert.not_valid_after} (days left: {days_left})")
