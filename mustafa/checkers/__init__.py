from collections import namedtuple

CheckResult = namedtuple(field_names='code message', typename='CheckResult')
