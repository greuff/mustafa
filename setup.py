from distutils.core import setup

setup(
	name='Mustafa',
	version='0.2',
	description='Mustafa monitoring',
	packages=['mustafa', 'mustafa.checkers', 'mustafa.notifiers'],
	license='MIT',
	author='Thomas Wana',
	author_email='thomas@wana.at',
	url='https://www.wana.at/',
	install_requires=['schedule', 'click'],
	extras_require={
		'Slack': ['slack_sdk'],
		'Telegram': ['telegram-send'],
		'SSH': ['paramiko'],
		'SSL': ['pyopenssl']
	}
)
